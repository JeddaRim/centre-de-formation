import React , {Component} from 'react';
import Navbarownprofile1 from '../layaout/NavbarOwnProfile1';
import '../../App.css';
import {connect} from 'react-redux'; 
import {signIn} from '../../store/actions/authAction';
import {Redirect} from 'react-router-dom';
import {Link} from 'react-router-dom';
import { TextField } from '@material-ui/core';
import Uploadb from './Uploadb';
import Button from '@material-ui/core/Button';


class Ajout extends Component {
    state = {
        email:'',
        password:''
    }

   
    handleChange =(e)=> {
        this.setState({
            [e.target.id]: e .target.value
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.signIn(this.state);
        //this.props.history.push('/Home') 
    }


    render(){
        
        const{authError , auth} = this.props;
        if (auth.uid) return <Redirect to ='/Home'/> 

        return(
            <div> <Navbarownprofile1/>
            <br></br> <br></br>
            <div className="container">
            <div className="body">
                <form onSubmit= {this.handleSubmit} className="form-style-10">
                    <h5 className="grey-text text-darken-3">Add Course   </h5>
                   
                    <div className="input-field">
                      <TextField id="standard-basic" label="Name Course" /> 
                    </div>
                   
                    <div className="input-field">
                    <TextField
                     id="outlined-multiline-static"
                     label="Description"
                     multiline
                     rows="3"
                     defaultValue=""
                     variant="outlined"
                     />
                    </div>

                    <div className="">
                    <Uploadb/>
                    </div>
<br></br>
                      <Button variant="outlined">Finish</Button>
                </form>
            </div>
            </div>
            </div>

        )
    }
    


}


const mapStateToProps = (state) => {
   return {
       authError: state.auth.authError,
       auth: state.firebase.auth
   } 
}



const mapDispatchToProps = (dispatch) => {
    return {
        signIn: (creds) => dispatch(signIn(creds))
    }
}


export default connect(mapStateToProps, mapDispatchToProps) 
(Ajout) 

